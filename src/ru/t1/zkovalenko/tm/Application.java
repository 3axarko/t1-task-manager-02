package ru.t1.zkovalenko.tm;

import ru.t1.zkovalenko.tm.constant.TerminalConst;

import java.util.Locale;

public class Application {

    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            System.out.println("Args is empty. Oh boy, oh my!");
            return;
        }
        final String arg = args[0];

        switch (arg.toLowerCase(Locale.ENGLISH)) {
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.ABOUT:
                showAbout();
                break;
            default:
                showError();
        }
    }

    public static void showError(){
        System.out.println("Wrong Argument");
    }

    public static void showVersion(){
        System.out.println("[VERSION]");
        System.out.println("1.2.0");
    }

    public static void showAbout(){
        System.out.println("[ABOUT]");
        System.out.println("Developer: 3axarko");
        System.out.println("And he's really good");
    }

    public static void showHelp(){
        System.out.println("[HELP]");
        System.out.printf("%s - Show app version \n", TerminalConst.VERSION);
        System.out.printf("%s - Who f** did it? And why? \n", TerminalConst.ABOUT);
        System.out.printf("%s - App commands \n", TerminalConst.HELP);
    }
    
}
